package it.dandandin.nihondin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NihondinActivity extends ListActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Intent myIntent = new Intent(NihondinActivity.this, Intro.class);
        startActivityForResult(myIntent, 0);
        
        if (!isOnline())
        {
        	new AlertDialog.Builder(this)
		        .setMessage(R.string.nonetlong)
		        .setTitle(R.string.nonettitle)
		        .setCancelable(true)
		        .setNeutralButton(android.R.string.ok,
		           new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int whichButton){
		        	   finish();
		        	   }
		           })
		        .show();
        }
        
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, 
        		android.R.layout.simple_list_item_1, android.R.id.text1);
        	setListAdapter(adapter);
        
    	new AsyncTask<Void, String, Void>()
    	{
     
    		@Override
    		protected Void doInBackground(Void... params)
    		{
    			try
    			{
    				JSONObject obj = getJSONObject(
        					"http://gdata.youtube.com/feeds/api/users/magneticdud/playlists?alt=jsonc&v=2");
    				JSONObject dataobject = obj.getJSONObject("data");
    				JSONArray jsonArray = dataobject.getJSONArray("items");
    				for (int i = 0; i < jsonArray.length(); i++)
    				{
    					JSONObject jsonObject = 
    						jsonArray.getJSONObject(i);
    					publishProgress(jsonObject.getString("title"));
    				}
    			}
    			catch (IOException ignored)
    			{
    			}
    			catch (JSONException ignored)
    			{
    			}
    			return null;
    		}
     
    		@Override
    		protected void onProgressUpdate(String... values)
    		{
    			for (String tweet : values)
    			{
    				adapter.add(tweet);
    			}
    		}
    	}.execute();
    	
    	adapter.notifyDataSetChanged();

        
        //setListAdapter(new ArrayAdapter(this, R.layout.list_item, this.youtubeplaylist()));
        
        
        //prende la lista hardcoded
        //setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, COUNTRIES));

        ListView lv = getListView();
        lv.setTextFilterEnabled(true);
        
//        lv.setOnItemClickListener(new OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View view,
//                int position, long id) {
//              // When clicked, show a toast with the TextView text
//              Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
//                  Toast.LENGTH_SHORT).show();
//            }
//        });
    }
    
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
        
    private static JSONObject getJSONObject(String url) throws 
		IOException, MalformedURLException, JSONException
	{
		HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
	 
		InputStream in = conn.getInputStream();
	 
		try
		{
			StringBuilder sb = new StringBuilder();
			BufferedReader r = new BufferedReader(
				new InputStreamReader(new DoneHandlerInputStream(in)));
			for (String line = r.readLine(); line != null; line = r.readLine())
			{
				sb.append(line);
			}
	 
			return new JSONObject(sb.toString());
		}
		finally
		{
			in.close();
		}
	}

}
