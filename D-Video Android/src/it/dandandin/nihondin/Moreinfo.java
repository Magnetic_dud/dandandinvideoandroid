package it.dandandin.nihondin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Moreinfo extends Activity {
	public void onCreate(Bundle savedInstanceState)
	   {
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.moreinfo);
	      Button avanti = (Button) findViewById(R.id.buttonavanti);
	      avanti.setOnClickListener(new View.OnClickListener() {
	         public void onClick(View view) {
	        	 Intent intent = new Intent();
	                setResult(RESULT_OK, intent);
	                finish();
	         } 
	      });
	   }
}
