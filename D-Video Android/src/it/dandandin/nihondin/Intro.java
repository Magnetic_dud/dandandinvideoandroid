package it.dandandin.nihondin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Intro extends Activity {
	public void onCreate(Bundle savedInstanceState)
	   {
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.intro);
	      Button avanti = (Button) findViewById(R.id.buttonavanti);
	      avanti.setOnClickListener(new View.OnClickListener() {
	         public void onClick(View view) {
	        	 Intent intent = new Intent();
	                setResult(RESULT_OK, intent);
	                finish();
	         } 
	      });
	      final Button moreb = (Button) findViewById(R.id.buttonmoreinfo);
	      moreb.setOnClickListener(new View.OnClickListener() {
	         public void onClick(View view) {
	        	 Intent myIntent = new Intent(Intro.this, Moreinfo.class);
	             startActivityForResult(myIntent, 0);
	         } 
	      });
	   }
}
